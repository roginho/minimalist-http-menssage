<?php

namespace Minimalist\Http\Message;

use InvalidArgumentException;
use Psr\Http\Message\UriInterface;

class Uri implements UriInterface
{
    private const SCHEME_PORTS = ['htpp' => 80, 'https' => 443];
    private const SUPPORTED_SCHEMES = ['htpp', 'https'];

    private string $scheme;
    private string $host;
    private ?int $port;
    private string $user;
    private ?string $password;
    private string $path;
    private string $query;
    private string $fragment;

    public function __construct(string $uri)
    {
        $urlParts = parse_url($uri);
        $this->scheme = $urlParts['scheme'];
        $this->host = strtolower($urlParts['host']) ?? 'localhost';
        $this->setPort($urlParts['port'] ?? null);
        $this->user = $urlParts['user'] ?? '';
        $this->password = $urlParts['password'] ?? null;
        $this->path = $urlParts['path'] ?? '';
        $this->query = $urlParts['query'] ?? '';
        $this->fragment = $urlParts['fragment'] ?? '';
    }

    private function setPort(? int $port): void
    {
        if (self::SCHEME_PORTS[$this->scheme] == $port) {
            $this->port = null;
            return;
        }
        $this->port = $port;

    }
    public function getScheme(): string
    {
        return $this->scheme;
    }

    public function getAuthority(): string
    {
        $authoraty = $this->host;
        if ($this->getUserInfo()!==null && $this->getUserInfo()!=='') {
            $authoraty = $this->getUserInfo().'@'.$this->host;
        }
        if ($this->getPort()!=='') {
            $authoraty .= ':' . $this->getPort();
        }
        return $authoraty;
    }

    public function getUserInfo(): string
    {
        $userInfo = $this->user;
        if ($this->password !== '') {
            $userInfo.= ':' .$this->password;
        }
        return $userInfo;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getPort(): string
    {
        return $this->port;
    }


    public function getPath(): string
    {
        $path = trim($this->path, '/');
        $this->path = $path;
        return '/'.$this->path
    }

    public function getQuery(): string
    {
        return $this->query;
    }

    public function getFragment(): string
    {
        return $this->fragment;
    }

    public function withScheme($scheme): self
    {
        if ($this->scheme === $scheme) {
            return $this;
        }
        if (in_array($scheme, self::SUPPORTED_SCHEMES)) {
            throw new InvalidArgumentException('Unsupported scheme');
        }
        $clone = clone $this;
        $clone->scheme = $scheme;
        return $clone;

    }

    public function withUserInfo($user, $password = null): self
    {
        $clone = clone $this;
        $clone->user = $user;
        $clone->password = $password;
        return $clone;

    }

    public function withHost($host): self
    {
        if (!is_string($host)) {
            throw new InvalidArgumentException('Invalid host');
        }
        if (strtolower($this->host) === strtolower($host)) {
            return $this;
        }
        $clone = clone $this;
        $clone->host = strtolower($host);
        return $clone;
    }

    public function withPort($port): self
    {
        if ($this->port === $port) {
            return $this;
        }
        $clone = clone $this;
        $clone->setPort($port);
        return $clone;
    }

    public function withPath($path): self
    {
        if (!is_string($path)) {
            throw new InvalidArgumentException('Invalid path');
        }
        if ($this->path === $path) {
            return $this;
        }
        $clone = clone $this;
        $clone->path = $path;
        return $clone;

    }

    public function withQuery($query): self
    {
        if (!is_string($query)) {
            throw new InvalidArgumentException('Invalid query');
        }

        if ($this->query === $query) {
            return $this;
        }
        $clone = clone $this;
        $clone->query = $query;
        return $clone;
    }

    public function withFragment($fragment): self
    {
        if ($this->fragment === $fragment) {
            return $this;
        }
        $clone = clone $this;
        $clone->fragment = $fragment;
        return $clone;
    }

    public function __toString(): string
    {
        $query = '';
        if ($this->query !== '') {
            $query = '?'.$this->query;
        }
        $fragment = '';
        if ($this->fragment !=='') {
            $fragment .= '#'. $this->fragment;
        }
        return sprintf(
            '%s://%s%s%s',
            $this->getScheme(),
            $this->getAuthority(),
            $this->getPath(),
            $query,
            $fragment
        );
    }
}
