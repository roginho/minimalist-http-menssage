<?php

namespace Minimalist\Http\Message;

use InvalidArgumentException;
use Psr\Http\Message\UriInterface;

trait RequestTrait
{
    protected const VALID_METHODS = ['get', 'post', 'put', 'delete', 'patch', 'head', 'option'];
    protected string $requestTarget;
    private string $method;
    private UriInterface $uri;

    private function setUri($uri): void
    {
        if(is_string($uri)){
            $uri = new Uri($uri);
        }
        $this->uri = $uri;
    }


    public function getRequestTarget(): string
    {
        return $this->requestTarget;
    }

    public function withRequestTarget($requestTarget): self
    {

        $requestTarget = strtolower($requestTarget);
        if($this->requestTarget === $requestTarget){
            return $this;
        }
        $clone = clone $this;
        $clone->requestTarget = $requestTarget;
        return $clone;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function withMethod($method): self
    {
        $method = strtolower($method);
        if($this->method === $method){
            return $this;
        }
        if(!in_array($method, self::VALID_METHODS))
        {
            throw new InvalidArgumentException('Only '. implode(',' , self::VALID_METHODS) .' are acceptable.');
        }
        $clone = clone $this;
        $clone->method = $method;
        return $clone;
    }

    public function getUri(): UriInterface
    {
        return $this->uri;
    }

    public function withUri(UriInterface $uri, $preserveHost = false)
    {
        $clone = clone $this;
        if($preserveHost){
            $newUri = $uri->withHost($this->uri->getHost());
            return $newUri;
        }
        $clone->uri = $uri;
        return $clone;
    }
}
